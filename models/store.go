package models

import (
	"time"
)

//Store docket 镜像仓库
type Store struct {
	ID         int
	Name       string
	Addr       string `orm:"unique"`
	UserName   string
	Password   string
	IsValid    bool
	Remark     string
	CreateTime time.Time `orm:"auto_now_add;type(datetime)"` // 创建时间
	Updated    time.Time `orm:"auto_now;type(datetime)"`
}
