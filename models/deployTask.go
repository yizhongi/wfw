package models

import (
	"time"
)

//DeployTask 部署任务
type DeployTask struct {
	ID            int `orm:"auto"`
	ImageName     string
	ContainerName string `orm:"unique"` //容器名称
	ContainerID   string
	OtherArg      string
	Remark        string
	HealthCheck   string
	HealthState   string
	TaskState     bool       `orm:"default(0);description(任务状态)"`
	CreateTime    time.Time  `orm:"auto_now_add;type(datetime)"` // 创建时间
	Updated       time.Time  `orm:"auto_now;type(datetime)"`
	Store         *Store     `orm:"rel(fk)"` //one To one
	Port          []*Port    `orm:"reverse(many)"`
	Volume        []*Volume  `orm:"reverse(many)"`
	TaskRun       []*TaskRun `orm:"reverse(many)"`
}

//Port 端口号
type Port struct {
	ID         int
	Local      int
	Container  int
	DeployTask *DeployTask `orm:"rel(fk)"`
}

//Volume 卷
type Volume struct {
	ID         int
	Local      string
	Container  string
	DeployTask *DeployTask `orm:"rel(fk)"`
}

//TaskRunEvent 记录部署任务的执行日志
type TaskRun struct {
	ID           int
	ImageID      string
	ImageVersion string
	ContainerID  string
	IsSuccess    bool
	IsOnline     bool
	Task         *DeployTask `orm:"rel(fk)"`
	CreateTime   time.Time   `orm:"auto_now_add;type(datetime)"` // 创建时间
}

//AppLog 应用日志
type AppLog struct {
	ID          int
	SourceModel string    //日志来源模块（表名or模块名）
	SourceID    int       //日志来源ID，关联具体的记录
	Content     string    //日志内容
	CreateTime  time.Time `orm:"auto_now_add;type(datetime)"` // 创建时间
}
