package models

//SysInfo 系统信息
type SysInfo struct {
	LocalIp    string `orm:"description(本机IP)"`
	InternetIp string `orm:"description(外网IP)"`
	DNS        string `orm:"description(DNS域名)"`
}
