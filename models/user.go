package models

import (
	"time"
)

//User 用户信息
type User struct {
	ID         int
	Username   string    `orm:"unique"` // 用户名
	Password   string    // 密码
	CreateTime time.Time `orm:"auto_now_add;type(datetime)"` // 创建时间
}
