package main

import (
	"fmt"
	"os/exec"

	"gitee.com/yizhongi/wfw/nginx"
)

func main() {
	test()
}

func test() {
	cmd := exec.Command("which", "nginx")
	output, err := cmd.Output()
	if err != nil {
		fmt.Println(err.Error())
	}
	// 因为结果是字节数组，需要转换成string
	fmt.Println(string(output))
	nginxClient := nginx.NewNginxClient()
	fmt.Println(nginxClient)
	nginxClient.Test()
	testParse()
}

func testParse() {
	c := `
		server {
			listen 443;
			server_name frp.xizhao.org;
			location / {
				proxy_pass http://193.112.33.161:488;
				
				proxy_redirect https://$host/ https://$http_host/;
				proxy_set_header X-Real-IP $remote_addr;
				proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
				proxy_set_header Host $host;
			}
			ssl_certificate /etc/nginx/certs/xizhao.org/cert;
			ssl_certificate_key /etc/nginx/certs/xizhao.org/key;
		}
	`
	b := []byte(c)
	block, err := nginx.Parse(b)
	if err != nil {
		fmt.Println(err.Error())
	}
	s := block.ToString("")
	fmt.Println(s)
}
