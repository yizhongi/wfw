package controllers

import (
	"github.com/beego/beego/v2/server/web"
)

type BaseController struct {
	web.Controller
}

type ReturnMsg struct {
	Code  int         `json:"code"`
	Msg   string      `json:"msg"`
	Count int64       `json:"count"`
	Data  interface{} `json:"data"`
}

func (this *BaseController) LayuiSuccessJson(count int64, data interface{}) {

	res := ReturnMsg{
		Code: 0, Msg: "", Count: count, Data: data,
	}
	this.Data["json"] = res
	this.ServeJSON() //对json进行序列化输出
	this.StopRun()
}

func (this *BaseController) SuccessJson(msg string, data interface{}) {

	res := ReturnMsg{
		Code: 200, Msg: msg, Data: data,
	}
	this.Data["json"] = res
	this.ServeJSON() //对json进行序列化输出
	this.StopRun()
}

func (this *BaseController) ErrorJson(code int, msg string, data interface{}) {

	res := ReturnMsg{
		Code: code, Msg: msg, Data: data,
	}

	this.Data["json"] = res
	this.ServeJSON() //对json进行序列化输出
	this.StopRun()
}
