package controllers

import (
	"gitee.com/yizhongi/wfw/util"
	"github.com/beego/beego/v2/server/web"
)

//WelcomeController 欢迎页控制器
type WelcomeController struct {
	web.Controller
}

//Get 响应欢迎页Get参数
// @router /welcome [get]
func (c *WelcomeController) Get() {
	//c.Layout = "layout.html"
	c.Data["Cpuinfos"] = util.GetCpuInfo()
	c.Data["MemInfo"] = util.GetMemInfo()
	//c.Data["DiskInfo"] = util.GetDiskInfo()
	//c.Data["HostInfo"] = util.GetHostInfo()
	c.Data["ServiceInfo"] = util.GetSysService()
	c.TplName = "welcome.html"
}
