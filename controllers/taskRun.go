package controllers

//TaskRunController 任务执行控制器
type TaskRunController struct {
	BaseController
}

//List
// @router /task-run [get]
func (c *TaskRunController) List() {
	c.TplName = "taskrun.html"
}

//TaskRunApiController 任务执行Api控制器
type TaskRunApiController struct {
	BaseController
}
