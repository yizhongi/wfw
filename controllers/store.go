package controllers

import (
	"strconv"

	"gitee.com/yizhongi/wfw/models"
	"github.com/beego/beego/v2/adapter/orm"
	"github.com/beego/beego/v2/server/web"
)

type StoreController struct {
	web.Controller
}

// @router /store [get]
func (c *StoreController) List() {
	//c.Layout = "layout.html"
	c.TplName = "store/store.html"
}

// @router /add-store [get]
func (c *StoreController) Add() {
	//c.Layout = "layout.html"
	c.TplName = "store/add_store.html"
}

// @router /edit-store/:id [get]
func (c *StoreController) Edit() {
	sid, _ := strconv.Atoi(c.Ctx.Input.Param(":id"))
	o := orm.NewOrm()
	s := models.Store{ID: sid}
	o.Read(&s)
	c.Data["d"] = s
	c.TplName = "store/edit_store.html"
}
