package controllers

import (
	"strconv"

	"gitee.com/yizhongi/wfw/models"
	"github.com/beego/beego/v2/adapter/orm"
	"github.com/beego/beego/v2/server/web"
)

type TaskController struct {
	web.Controller
}

// @router /tasks [get]
func (c *TaskController) List() {
	//c.Layout = "layout.html"
	c.TplName = "task/tasks.html"
}

// @router /add-task [get]
func (c *TaskController) Add() {
	//c.Layout = "layout.html"
	c.TplName = "task/add_task.html"
}

// @router /edit-task/:task_id [get]
func (c *TaskController) Edit() {
	//c.Layout = "layout.html"
	tid, _ := strconv.Atoi(c.Ctx.Input.Param(":task_id"))
	o := orm.NewOrm()
	t := models.DeployTask{ID: tid}
	o.Read(&t)
	if t.Store != nil {
		o.Read(t.Store)
	}
	o.LoadRelated(&t, "Port")
	o.LoadRelated(&t, "Volume")
	c.Data["d"] = t
	c.TplName = "task/edit_task.html"
}

// @router /run-task/:task_id [get]
func (c *TaskController) Run() {
	//c.Layout = "layout.html"
	tid, _ := strconv.Atoi(c.Ctx.Input.Param(":task_id"))
	o := orm.NewOrm()
	t := models.DeployTask{ID: tid}
	o.Read(&t)
	if t.Store != nil {
		o.Read(t.Store)
	}
	o.LoadRelated(&t, "Port")
	o.LoadRelated(&t, "Volume")
	c.Data["d"] = t
	c.TplName = "task/run_task.html"
}
