package routers

import (
	beego "github.com/beego/beego/v2/server/web"
	"github.com/beego/beego/v2/server/web/context/param"
)

func init() {

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:StoreApiController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:StoreApiController"],
        beego.ControllerComments{
            Method: "Add",
            Router: "/add",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:StoreApiController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:StoreApiController"],
        beego.ControllerComments{
            Method: "Edit",
            Router: "/edit",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:StoreApiController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:StoreApiController"],
        beego.ControllerComments{
            Method: "List",
            Router: "/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:StoreApiController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:StoreApiController"],
        beego.ControllerComments{
            Method: "Login",
            Router: "/login/?:id",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:StoreController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:StoreController"],
        beego.ControllerComments{
            Method: "Add",
            Router: "/add-store",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:StoreController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:StoreController"],
        beego.ControllerComments{
            Method: "Edit",
            Router: "/edit-store/:id",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:StoreController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:StoreController"],
        beego.ControllerComments{
            Method: "List",
            Router: "/store",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskApiController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskApiController"],
        beego.ControllerComments{
            Method: "AddTask",
            Router: "/add",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskApiController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskApiController"],
        beego.ControllerComments{
            Method: "Delete",
            Router: "/del/?:id",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskApiController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskApiController"],
        beego.ControllerComments{
            Method: "Deploy",
            Router: "/deploy",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskApiController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskApiController"],
        beego.ControllerComments{
            Method: "Edit",
            Router: "/edit",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskApiController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskApiController"],
        beego.ControllerComments{
            Method: "GetWebHookAddr",
            Router: "/hook/?:taskID",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskApiController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskApiController"],
        beego.ControllerComments{
            Method: "ListTasks",
            Router: "/list",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskController"],
        beego.ControllerComments{
            Method: "Add",
            Router: "/add-task",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskController"],
        beego.ControllerComments{
            Method: "Edit",
            Router: "/edit-task/:task_id",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskController"],
        beego.ControllerComments{
            Method: "Run",
            Router: "/run-task/:task_id",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskController"],
        beego.ControllerComments{
            Method: "List",
            Router: "/tasks",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskRunController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:TaskRunController"],
        beego.ControllerComments{
            Method: "List",
            Router: "/task-run",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:UserLoginController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:UserLoginController"],
        beego.ControllerComments{
            Method: "Get",
            Router: "/login",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:UserLoginController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:UserLoginController"],
        beego.ControllerComments{
            Method: "Post",
            Router: "/login",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:UserLogoutController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:UserLogoutController"],
        beego.ControllerComments{
            Method: "Get",
            Router: "/logout",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:UserRegisterController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:UserRegisterController"],
        beego.ControllerComments{
            Method: "Get",
            Router: "/register",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:UserRegisterController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:UserRegisterController"],
        beego.ControllerComments{
            Method: "Post",
            Router: "/register",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:WebhookController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:WebhookController"],
        beego.ControllerComments{
            Method: "Info",
            Router: "/hookinfo",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:WebhookController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:WebhookController"],
        beego.ControllerComments{
            Method: "NewIp",
            Router: "/newip",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:WebhookController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:WebhookController"],
        beego.ControllerComments{
            Method: "NewToken",
            Router: "/newtoken",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:WebhookController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:WebhookController"],
        beego.ControllerComments{
            Method: "SaveDomain",
            Router: "/savedomain",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:WebhookController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:WebhookController"],
        beego.ControllerComments{
            Method: "Index",
            Router: "/webhook",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:WelcomeController"] = append(beego.GlobalControllerRouter["gitee.com/yizhongi/wfw/controllers:WelcomeController"],
        beego.ControllerComments{
            Method: "Get",
            Router: "/welcome",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
