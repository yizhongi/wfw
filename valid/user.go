package valid

import (
	"errors"

	"github.com/astaxie/beego/validation"
)

type ValidateUser struct {
	Username string `alias:"用户名" valid:"Required;MinSize(3);MaxSize(20)"`
	Password string `alias:"密码" valid:"Required;MinSize(3);MaxSize(20)"`
}

func (u *ValidateUser) ValidUser() (err error) {
	// 先创建一个Validation
	valid := validation.Validation{}
	// 验证传入的User信息
	status, _ := valid.Valid(u)
	// 如果验证失败
	if !status {
		for _, err := range valid.Errors {
			// 获取字段别称
			var alias = GetAlias(ValidateUser{}, err.Field)
			// 返回验证的错误信息
			return errors.New(alias + err.Message)
		}
	}
	return nil
}
