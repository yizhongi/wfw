package nginx

import (
	"fmt"
	"io/ioutil"
)

type NginxConfig struct {
	http string
}

func LoadConfig(path string) {
	content, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Println(err.Error())
	}

	block, err := Parse(content)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(block)
}
