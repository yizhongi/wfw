// +build !windows

//Package nginx ...
package nginx

import (
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"strconv"
	"strings"
	"time"

	"gitee.com/yizhongi/wfw/util"
	"github.com/shirou/gopsutil/host"
	pnet "github.com/shirou/gopsutil/net"
	"github.com/shirou/gopsutil/process"
)

//Memory ..
type Memory struct {
	Percent     float32 `json:"percent"`
	VirtualSize uint64  `json:"virtual_byte"`
	RealSize    uint64  `json:"real_byte"`
}

//Status ..
type Status struct {
	PID      int32          `json:"pid"`
	CPU      float32        `json:"cpu"`
	Memory   *Memory        `json:"memory"`
	Status   string         `json:"status"`
	Start    string         `json:"start_at"`
	Time     float32        `json:"time"`
	Host     *host.InfoStat `json:"host"`
	Subpid   []int32        `json:"sub_pid"`
	IPAddrs  []string       `json:"ip_address"`
	Networks *Networks      `json:"networks"`
}

//Process ..
type Process struct {
	Pid     int32
	Process *process.Process
}

//Addr ..
type Addr struct {
	IP   string `json:"ip"`
	Port uint32 `json:port`
}

//Network ..
type Network struct {
	Stat  string `json:"stat"`
	Laddr string `json:"local_address"`
	Raddr string `json:"remote_address"`
}

//Networks ..
type Networks struct {
	Network    []*Network            `json:"network"`
	IOCounters []pnet.IOCountersStat `json:"io_counters"`
	Total      map[string]int        `json:"total"`
}

// String returns JSON value of the memory info
func (obj *Memory) String() string {
	str, _ := json.Marshal(obj)
	return string(str)
}

// String returns JSON value of the status info
func (obj *Status) String() string {
	str, _ := json.Marshal(obj)
	return string(str)
}

//New .
func (c *Process) New(pid int32) (*Process, error) {
	if pexis, _ := process.PidExists(int32(pid)); pexis == false {
		return nil, errors.New("进程ID不存在")
	}
	proc, err := process.NewProcess(int32(pid))
	if err != nil {
		return nil, err
	}
	self := &Process{
		Pid:     proc.Pid,
		Process: proc,
	}
	return self, nil
}

//Cpu CPU占用比例
func (c *Process) Cpu() float64 {
	proc := c.Process
	cpu, _ := proc.CPUPercent()
	return cpu
}

//Memory 内存信息
func (c *Process) Memory() *Memory {
	proc := c.Process
	// 内存信息
	mem_info, _ := proc.MemoryInfo()
	// 内存占用比例
	m_percent, _ := proc.MemoryPercent()
	// 内存详情
	mem := &Memory{
		Percent:     m_percent,
		VirtualSize: mem_info.VMS,
		RealSize:    mem_info.RSS,
	}
	return mem
}

//Status 运行状态
func (c *Process) Status() string {
	status, _ := c.Process.Status()
	return status
}

//CreateTime 启动时间
func (c *Process) CreateTime() int64 {
	start_f, _ := c.Process.CreateTime()
	return start_f
}

//StartDateTime 启动时间格式化
func (c *Process) StartDateTime() string {
	start_f := c.CreateTime()
	start := time.Unix(start_f/1000, start_f).Format(time.RFC3339)
	return start
}

//Time 运行时长
func (c *Process) Time() float32 {
	start_f := c.CreateTime()
	ttl_f := time.Now().Sub(time.Unix(start_f/1000, start_f)).Seconds()
	//ttl := fmt.Sprintf("%.5f", ttl_f)
	return float32(ttl_f)
}

//Host 主机名
func (c *Process) Host() *host.InfoStat {
	host, _ := host.Info()
	return host
}

//Children 子进程 PID 列表
func (c *Process) Children() []int32 {
	subs := make([]int32, 0)
	pid := c.Process.Pid
	procs, err := process.Processes()
	if err != err {
		return subs
	}
	for _, sub := range procs {
		if ppid, _ := sub.Ppid(); ppid == pid {
			subs = append(subs, int32(sub.Pid))
		}
	}
	return subs
}

//Internal 获取网卡IP
func (c *Process) Internal() []string {
	addrs, err := net.InterfaceAddrs()
	ip := make([]string, 0)
	if err != nil {
		return ip
	}
	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				ip = append(ip, ipnet.IP.String())
			}
		}
	}
	return ip
}

//Networks 获取网络情况
func (c *Process) Networks() *Networks {
	pid := c.Process.Pid
	status_list := map[string]int{"LISTEN": 0, "ESTABLISHED": 0, "TIME_WAIT": 0, "CLOSE_WAIT": 0, "LAST_ACK": 0, "SYN_SENT": 0}
	//status_list := &NetTotal{LISTEN: 0, ESTABLISHED: 0, TIME_WAIT: 0, CLOSE_WAIT: 0, LAST_ACK: 0, SYN_SENT: 0}

	/**
	networks := &Networks{
		LISTEN:      make([]*Network, 0),
		ESTABLISHED: make([]*Network, 0),
		TIME_WAIT:   make([]*Network, 0),
		CLOSE_WAIT:  make([]*Network, 0),
		LAST_ACK:    make([]*Network, 0),
		SYN_SENT:    make([]*Network, 0),
		Total:       status_list,
	}
	*/
	networks := make([]*Network, 0)
	pn, _ := pnet.ConnectionsPid("tcp", int32(pid))
	for _, sub_pc := range pn {
		net := &Network{
			Stat:  sub_pc.Status,
			Laddr: sub_pc.Laddr.IP + ":" + fmt.Sprintf("%d", sub_pc.Laddr.Port),
			Raddr: sub_pc.Raddr.IP + ":" + fmt.Sprintf("%d", sub_pc.Raddr.Port),
			//Laddr: &Addr{IP: sub_pc.Laddr.IP, Port: sub_pc.Laddr.Port},
			//Raddr: &Addr{IP: sub_pc.Raddr.IP, Port: sub_pc.Raddr.Port},
		}
		status := string(sub_pc.Status)
		networks = append(networks, net)
		status_list[status] += 1
	}
	n, _ := c.Process.NetIOCounters(false)
	return &Networks{
		Network:    networks,
		Total:      status_list,
		IOCounters: n,
	}
}

//getPid 获取 Nginx 进程PID
func getPid(c *Nginx) (int32, error) {
	if exists := util.Exists(c.Pid); exists == false {
		return int32(0), errors.New("PID文件不存在 : " + c.Pid)
	}
	fPid, err := util.Get(c.Pid)
	if err != nil {
		return int32(0), err
	}
	sPid := strings.Replace(fPid, "\n", "", -1)
	sPid = fmt.Sprintf("%s", sPid)
	if sPid == "" {
		return int32(0), errors.New("PID不存在 : " + c.Pid)
	}
	pid, err := strconv.Atoi(sPid)
	return int32(pid), err
}
