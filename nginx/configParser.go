package nginx

import (
	"container/list"
	"fmt"
	"strings"
	"sync"
)

// ConfigureBlock represent a block in nginx configure file.
// The content of a nginx configure file should be a block.
type ConfigureBlock []ConfigureCommand

// ToString  ..
func (b *ConfigureBlock) ToString(prefix string) string {
	var ret string
	for _, c := range *b {
		if len(c.Block) == 0 {
			ret = ret + prefix + strings.Join(c.Words, " ") + ";\n"
		} else {
			ret = ret + fmt.Sprintf("%s%s {\n %s \n%s}\n", prefix, strings.Join(c.Words, " "), c.Block.ToString(prefix+"    "), prefix)
		}
	}
	return ret
}

// ConfigureCommand represenct a command in nginx configure file.
type ConfigureCommand struct {
	// Words compose the command
	Words []string
	// Block follow the command
	Block ConfigureBlock
}

// ToString 序列化
func (c *ConfigureCommand) ToString() {
	for _, b := range c.Block {
		fmt.Println(b.Words)
	}
}

func serialize(block ConfigureBlock) string {
	var ret string
	for _, b := range block {
		ret = ret + fmt.Sprintf("%s{\n %s \n }", b.Words, serialize(b.Block))
	}
	return ret
}

type parser struct {
	sync.Mutex
	*scanner
}

var (
	emptyBlock   = ConfigureBlock(nil)
	emptyCommand = ConfigureCommand{}
)

// Parse the content of nginx configure file into NginxConfigureBlock
func Parse(content []byte) (blk ConfigureBlock, err error) {
	var p parser
	return p.parse(content)
}

func (p *parser) parse(content []byte) (blk ConfigureBlock, err error) {
	p.Lock()
	defer p.Unlock()
	defer func() {
		if e := recover(); e != nil {
			err = e.(error)
		}
	}()
	p.scanner = newScanner(content)
	cmds := list.New()
ForLoop:
	for {
		token := p.scan()
		switch token.typ {
		case eof:
			break ForLoop
		case word:
			cmd, err := p.scanCommand(token.lit)
			if err != nil {
				return nil, err
			}
			cmds.PushBack(cmd)
		case comment:
			continue
		default:
			return nil, fmt.Errorf("unexpected global token %s at line %d", token.typ, p.line)
		}
	}
	cfg := make([]ConfigureCommand, cmds.Len())
	for i, cmd := 0, cmds.Front(); cmd != nil; i, cmd = i+1, cmd.Next() {
		cfg[i] = cmd.Value.(ConfigureCommand)
	}
	return cfg, nil
}

func (p *parser) scanCommand(startWord string) (ConfigureCommand, error) {
	words := list.New()
	if startWord != "" {
		words.PushBack(startWord)
	}
	var err error
	var block ConfigureBlock
ForLoop:
	for {
		token := p.scan()
		switch token.typ {
		case eof:
			return emptyCommand, fmt.Errorf("missing terminating token at line %d", p.line)
		case braceOpen:
			block, err = p.scanBlock()
			if err != nil {
				return emptyCommand, err
			}
			break ForLoop
		case semicolon:
			break ForLoop
		case comment:
			continue
		case word:
			words.PushBack(token.lit)
		default:
			return emptyCommand, fmt.Errorf("unexpected command token %s at line %d", token.typ, p.line)
		}
	}
	cmd := ConfigureCommand{
		Words: make([]string, words.Len()),
		Block: block,
	}
	for i, word := 0, words.Front(); word != nil; i, word = i+1, word.Next() {
		cmd.Words[i] = word.Value.(string)
	}
	return cmd, nil
}

func (p *parser) scanBlock() (ConfigureBlock, error) {
	cmds := list.New()
ForLoop:
	for {
		token := p.scan()
		switch token.typ {
		case eof:
			return emptyBlock, fmt.Errorf("missing terminating token at line %d", p.line)
		case braceClose:
			break ForLoop
		case comment:
			continue
		case word:
			cmd, err := p.scanCommand(token.lit)
			if err != nil {
				return emptyBlock, err
			}
			cmds.PushBack(cmd)
		default:
			return emptyBlock, fmt.Errorf("unexpected block token %s at line %d", token.typ, p.line)
		}
	}
	block := make([]ConfigureCommand, cmds.Len())
	for i, cmd := 0, cmds.Front(); cmd != nil; i, cmd = i+1, cmd.Next() {
		block[i] = cmd.Value.(ConfigureCommand)
	}
	return block, nil
}

func (p *parser) saveToFile(fileName string, blk ConfigureBlock) {

}
