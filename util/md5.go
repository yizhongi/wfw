package util

import (
	"crypto/md5"
	"encoding/hex"
)

//Md5Pwd 用来对密码字符串进行md5计算
func Md5Pwd(str string) string {
	h := md5.New()
	h.Write([]byte(str))
	return hex.EncodeToString(h.Sum(nil))
}
