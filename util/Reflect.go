package util

import (
	"encoding/json"
	"fmt"
	"reflect"
)

//CopyStructValue 复制结构体的值
func CopyStructValue(src interface{}, dst interface{}) {
	dstType := reflect.TypeOf(dst)
	if dstType.Kind() != reflect.Ptr {
		panic("dst is not a ptr")
	}
	aj, _ := json.Marshal(src)
	err := json.Unmarshal(aj, dst)
	fmt.Println(err)
}
