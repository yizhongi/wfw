package util

//LogType 日志类型
type LogType int

const (
	//InfoLog 信息记录log
	InfoLog LogType = 0
	//DebugLog 调试日志
	DebugLog LogType = 1
	//ErrorLog 错误日志
	ErrorLog LogType = 2
)

//SaveLog 保存日志
func SaveLog(sourceModel string, sourceID string, content string, logtype LogType) {

}

//GetLog 获取日志内容
func GetLog(sourceModel string, sourceID string, logtype LogType) []string {
	return nil
}
