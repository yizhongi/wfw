package util

import (
	"github.com/beego/beego/v2/server/web"
)

var confFile = "conf/app.conf"
var runmode = ""

func init() {
	val, _ := web.AppConfig.String("runmode")
	runmode = val
}

//NewValue 获取新配置项值的方法
type NewValue func() string

//GetConfig 从配置文件中取出配置值
func GetConfig(isNew bool, key string, newValue NewValue) string {
	if isNew && newValue != nil {
		web.AppConfig.Set(key, newValue())
		web.AppConfig.SaveConfigFile(confFile)
	}
	val, err := web.AppConfig.String(runmode + "::" + key)
	if (err != nil || val == "") && newValue != nil {
		t := newValue()
		web.AppConfig.Set(key, t)
		web.AppConfig.SaveConfigFile(confFile)
		return t
	}
	return val
}

//SetConfig 设置配置项
func SetConfig(key string, value string) {
	web.AppConfig.Set(key, value)
	web.AppConfig.SaveConfigFile(confFile)
}
