package util

import (
	"errors"
)

//ContainerOption 容器选项，标识一个容器的特性
type ContainerOption struct {
	ImageName string
	//PublishPorts key代表容器端口，value代表宿主机端口
	PublishPorts map[string]string
	//Volume key代表容器目录卷，value代表宿主机目录卷
	Volume        map[string]string
	Hostname      string
	ContainerName string
	Restart       ContainerRestartType
}

//ToArg 把容器特性转换为参数数组格式
func (c *ContainerOption) ToArg() []string {
	if c.ImageName == "" {
		panic(errors.New("ImageName is not empty string"))
	}
	var args = []string{}
	if c.ContainerName != "" {
		args = append(args, "-n", c.ContainerName)
	}
	if c.Hostname != "" {
		args = append(args, "-h", c.Hostname)
	}
	if len(c.PublishPorts) > 0 {
		for k, v := range c.PublishPorts {
			args = append(args, "-p", k+":"+v)
		}
	}
	if len(c.Volume) > 0 {
		for k, v := range c.PublishPorts {
			args = append(args, "-v", k, v)
		}
	}
	if c.Restart != CRNo && c.Restart != "" {
		args = append(args, "--restart=\""+string(c.Restart)+"\"")
	}
	args = append(args, c.ImageName)
	return args
}
