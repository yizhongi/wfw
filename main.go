package main

import (
	_ "gitee.com/yizhongi/wfw/routers"
	_ "gitee.com/yizhongi/wfw/valid"
	"github.com/beego/beego/v2/server/web"
)

func main() {
	web.Run()
}
